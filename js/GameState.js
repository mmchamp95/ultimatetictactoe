// returns a clone of the passed object
function clone(obj) {
	return JSON.parse(JSON.stringify(obj));
}

// returns an object with the inputted move coordinates
function Move(outerX, outerY, innerX, innerY) {
	return {
		outerX: outerX,
		outerY: outerY,
		innerX: innerX,
		innerY: innerY
	};
}

// game state object constructor
function GameState(options) {
	// set selected cells
	if (options.selected !== undefined) {
		this._selected = options.selected;
	} else {
		this._selected = [
	         [
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
	         ],
	         [
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
	         ],
	         [
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]],
	             [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
	         ]
	    ];
	}

	// set won outer cells
	if (options.wonCells !== undefined) {
		this._wonCells = options.wonCells;
	} else {
		this._wonCells = [[0, 0, 0], [0, 0, 0], [0, 0, 0]];
	}

	// set the last move coordinates
	this.lastMove = options.lastMove;

	// set the active player
	this.activePlayer = options.activePlayer || 1;
}

// game state object functions
GameState.prototype = {
	// applies the inputed move
	move: function (outerX, outerY, innerX, innerY) {
		this._selected[outerX][outerY][innerX][innerY] = this.activePlayer;
		this._wonCells[outerX][outerY] = this.boardWin(outerX, outerY, this.activePlayer);
		this.lastMove = Move(outerX, outerY, innerX, innerY);
		this.activePlayer = this.activePlayer === 1 ? 2 : 1;
	},

	// returns a new game state with the specified move
	newStateFromMove: function (move) {
		// copy current game state
		var gameState = new GameState({
			selected: clone(this._selected),
			wonCells: clone(this._wonCells),
			lastMove: clone(this.lastMove),
			activePlayer: this.activePlayer
		});

		// make the move
		gameState.move(move.outerX, move.outerY, move.innerX, move.innerY);

		// return new game state
		return gameState;
	},

	// returns the coordinates of all available moves
	getMoves: function () {
		var outerX, outerY,
			moves = [];

		// gets the moves for an inner board
		function getInnerBoardMoves(selected, outerX, outerY) {
			var innerX, innerY;

			for (innerX = 0; innerX < 3; innerX++) {
		        for (innerY = 0; innerY < 3; innerY++) {
		            if (selected[outerX][outerY][innerX][innerY] === 0) {
		                moves.push(Move(outerX, outerY, innerX, innerY));
		            }
		        }
		    }
		}

		// get valid moves based on last move
		if (this.lastMove.innerX !== -1 && this._wonCells[this.lastMove.innerX][this.lastMove.innerY] === 0) {
			getInnerBoardMoves(this._selected, this.lastMove.innerX, this.lastMove.innerY);
		} else {
			for (outerX = 0; outerX < this._selected.length; outerX++) {
				for (outerY = 0; outerY < this._selected[outerX].length; outerY++) {
					if (this._wonCells[outerX][outerY] === 0) {
						getInnerBoardMoves(this._selected, outerX, outerY);
					}
				}
			}
		}

		// return array of move objects
		return moves;
	},

	// returns whether a board has been won by a player,
	// returns the winning player, -1 if there is a tie or 0 if neither
	boardWin: function (outerX, outerY, player) {
		var innerX, innerY;

	    if ((
			this._selected[outerX][outerY][0][0] == player &&
			this._selected[outerX][outerY][0][1] == player &&
			this._selected[outerX][outerY][0][2] == player
		) || (
			this._selected[outerX][outerY][1][0] == player &&
			this._selected[outerX][outerY][1][1] == player &&
			this._selected[outerX][outerY][1][2] == player
		) || (
			this._selected[outerX][outerY][2][0] == player &&
			this._selected[outerX][outerY][2][1] == player &&
			this._selected[outerX][outerY][2][2] == player
		) || (
			this._selected[outerX][outerY][0][0] == player &&
			this._selected[outerX][outerY][1][0] == player &&
			this._selected[outerX][outerY][2][0] == player
		) || (
			this._selected[outerX][outerY][0][1] == player &&
			this._selected[outerX][outerY][1][1] == player &&
			this._selected[outerX][outerY][2][1] == player
		) || (
			this._selected[outerX][outerY][0][2] == player &&
			this._selected[outerX][outerY][1][2] == player &&
			this._selected[outerX][outerY][2][2] == player
		) || (
			this._selected[outerX][outerY][0][0] == player &&
			this._selected[outerX][outerY][1][1] == player &&
			this._selected[outerX][outerY][2][2] == player
		) || (
			this._selected[outerX][outerY][0][2] == player &&
			this._selected[outerX][outerY][1][1] == player &&
			this._selected[outerX][outerY][2][0] == player
		)) {
			return player;
		}

	    for (innerX = 0; innerX < 3; innerX++) {
	        for (innerY = 0; innerY < 3; innerY++) {
	            if (this._selected[outerX][outerY][innerX][innerY] === 0) {
					return 0;
				}
	        }
	    }

	    return -1;
	},

	// returns whether the game has been won by a player,
	// returns the winning player, -1 if there is a tie or 0 if neither
	win: function (player) {
		var x, y;

		if ((
			this._wonCells[0][0] == player &&
			this._wonCells[0][1] == player &&
			this._wonCells[0][2] == player
		) || (
	        this._wonCells[1][0] == player &&
			this._wonCells[1][1] == player &&
			this._wonCells[1][2] == player
		) || (
			this._wonCells[2][0] == player &&
			this._wonCells[2][1] == player &&
			this._wonCells[2][2] == player
		) || (
			this._wonCells[0][0] == player &&
			this._wonCells[1][0] == player &&
			this._wonCells[2][0] == player
		) || (
			this._wonCells[0][1] == player &&
			this._wonCells[1][1] == player &&
			this._wonCells[2][1] == player
		) || (
			this._wonCells[0][2] == player &&
			this._wonCells[1][2] == player &&
			this._wonCells[2][2] == player
		) || (
			this._wonCells[0][0] == player &&
			this._wonCells[1][1] == player &&
			this._wonCells[2][2] == player
		) || (
			this._wonCells[0][2] == player &&
			this._wonCells[1][1] == player &&
			this._wonCells[2][0] == player
		)) {
			return player;
		}


		for (x = 0; x < 3; x++) {
	        for (y = 0; y < 3; y++) {
	            if (this._wonCells[x][y] === 0) {
					return 0;
				}
	        }
	    }

	    return -1;
	},

	// returns the number of boards won for a player
	numWins: function (player) {
		var x, y,
			wins = 0;

		for (x = 0; x < 3; x++) {
	        for (y = 0; y < 3; y++) {
	            if (this._wonCells[x][y] === player) {
					wins++;
				}
	        }
	    }

		return wins;
	},

	// returns the number of potential wins in each board for a player
	potentialBoardWins: function (player) {
		var outerX, outerY,
			potential = 0;

		// checks for arrangements that can be won in 1 move
		function checkBoard(selected, outerX, outerY) {
			// top row
			if ((
				selected[outerX][outerY][0][0] == player &&
				(selected[outerX][outerY][1][0] == player || selected[outerX][outerY][2][0])
			) ||
				selected[outerX][outerY][1][0] == player && selected[outerX][outerY][2][0]
			) {
				potential++;
			}

			// middle row
			if ((
				selected[outerX][outerY][0][1] == player &&
				(selected[outerX][outerY][1][1] == player || selected[outerX][outerY][2][1])
			) ||
				selected[outerX][outerY][1][1] == player && selected[outerX][outerY][2][1]
			) {
				potential++;
			}

			// bottom row
			if ((
				selected[outerX][outerY][0][2] == player &&
				(selected[outerX][outerY][1][2] == player || selected[outerX][outerY][2][2])
			) ||
				selected[outerX][outerY][1][2] == player && selected[outerX][outerY][2][2]
			) {
				potential++;
			}

			// left column
			if ((
				selected[outerX][outerY][0][0] == player &&
				(selected[outerX][outerY][0][1] == player || selected[outerX][outerY][0][2])
			) ||
				selected[outerX][outerY][0][1] == player && selected[outerX][outerY][0][2]
			) {
				potential++;
			}

			// middle column
			if ((
				selected[outerX][outerY][1][0] == player &&
				(selected[outerX][outerY][1][1] == player || selected[outerX][outerY][1][2])
			) ||
				selected[outerX][outerY][1][1] == player && selected[outerX][outerY][1][2]
			) {
				potential++;
			}

			// right column
			if ((
				selected[outerX][outerY][2][0] == player &&
				(selected[outerX][outerY][2][1] == player || selected[outerX][outerY][2][2])
			) ||
				selected[outerX][outerY][2][1] == player && selected[outerX][outerY][2][2]
			) {
				potential++;
			}

			// top left to bottom right
			if ((
				selected[outerX][outerY][0][0] == player &&
				(selected[outerX][outerY][1][1] == player || selected[outerX][outerY][2][2])
			) ||
				selected[outerX][outerY][1][1] == player && selected[outerX][outerY][2][2]
			) {
				potential++;
			}

			// bottom right to top left
			if ((
				selected[outerX][outerY][0][2] == player &&
				(selected[outerX][outerY][1][1] == player || selected[outerX][outerY][2][0])
			) ||
				selected[outerX][outerY][1][1] == player && selected[outerX][outerY][2][0]
			) {
				potential++;
			}
		}

		// go through each board
		for (outerX = 0; outerX < 3; outerX++) {
			for (outerY = 0; outerY < 3; outerY++) {
				checkBoard(this._selected, outerX, outerY);
			}
		}

		return potential;
	},

	// returns if a board has been completed
	isFinal: function (outerX, outerY) {
		return this._wonCells[outerX][outerY] !== 0;
	},

	// returns if the game is over
	over: function () {
		if (this.win(1) !== 0 || this.win(2) !== 0) {
			return true;
		}

		return false;
	},

	// returns a string representation of the game state
	toString: function () {
		var outerX, outerY, innerX, innerY,
			string = '';

		for (outerY = 0; outerY < 3; outerY++) {
			for (innerY = 0; innerY < 3; innerY++) {
				for (outerX = 0; outerX < 3; outerX++) {
					for (innerX = 0; innerX < 3; innerX++) {
						string += this._selected[outerX][outerY][innerX][innerY] + '&nbsp&nbsp';
					}
					string += '&nbsp&nbsp&nbsp';
				}
				string += '<br>';
			}
			string += '<br>';
		}

		for (outerY = 0; outerY < 3; outerY++) {
			for (outerX = 0; outerX < 3; outerX++) {
				string += this._wonCells[outerX][outerY] + '&nbsp&nbsp';
			}
			string += '<br>';
		}

		string += 'win=' + this.win();

		return string + '<br>';
	}
};
