// simulates a "dumb" AI
function dumbAI(gameState, maximizingPlayer) {
	var opponent = maximizingPlayer === 1 ? 2 : 1,
		bestMove;

	// returns the minimum scored move
	function min(moves) {
		var i = 0,
			move = moves[i];

		for (i = 1; i < moves.length; i++) {
			if (moves[i].score < move.score) {
				move = moves[i];
			}
		}

		return move;
	}

	// returns the maximum scored move
	function max(moves) {
		var i = 0,
			move = moves[i];

		for (i = 1; i < moves.length; i++) {
			if (moves[i].score > move.score) {
				move = moves[i];
			}
		}

		return move;
	}

	// returns a score for the maximizing player based on the game state and depth
	function score(gameState, depth) {
		var score = 0;

		if (gameState.win(maximizingPlayer) > 0) {
			score += 1000;
		} else if (gameState.win(opponent) > 0) {
			score -= 1000;
		} else {
			score += gameState.numWins(maximizingPlayer) * 100;
			score -= gameState.numWins(opponent) * 100;
		}

		return score + depth;
	}

	// returns the best score for a player based on the current game state
	(function minMax(gameState, depth, player) {
		var moves = [],
			newGameState,
			move,
			i;

		if (depth === 0 || gameState.over()) {
			return score(gameState, depth);
		}

		moves = gameState.getMoves();
		if (player === maximizingPlayer) {
			for (i = 0; i < moves.length; i++) {
				newGameState = gameState.newStateFromMove(moves[i]);
				moves[i].score = minMax(newGameState, depth - 1, opponent);
			}
			move = max(moves);
		} else {
			for (i = 0; i < moves.length; i++) {
				newGameState = gameState.newStateFromMove(moves[i]);
				moves[i].score = minMax(newGameState, depth - 1, maximizingPlayer);
			}
			move = min(moves);
		}

		bestMove = move;
		return move.score;
	})(gameState, 5, maximizingPlayer);

	return bestMove;
}

onmessage = function (e) {
	importScripts('../GameState.js');
	postMessage(
		dumbAI(new GameState({
			selected: e.data.selected,
			wonCells: e.data.wonCells,
			lastMove: e.data.lastMove,
			activePlayer: e.data.maximizingPlayer
		}), e.data.maximizingPlayer)
	);
};
