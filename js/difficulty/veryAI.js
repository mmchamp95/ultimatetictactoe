// simulates a very intelligent AI
function veryAI(gameState, maximizingPlayer) {
	var opponent = maximizingPlayer === 1 ? 2 : 1,
		bestMove;

	// returns the minimum scored move
	function min(move1, move2) {
		if (move1.score < move2.score) {
			return clone(move1);
		}
		return clone(move2);
	}

	// returns the maximum scored move
	function max(move1, move2) {
		if (move1.score > move2.score) {
			return clone(move1);
		}
		return clone(move2);
	}

	// returns a score for the maximizing player based on the game state and depth
	function score(gameState, depth) {
		var score = 0,
			lastMove;

		// most points for winning the game
		if (gameState.win(maximizingPlayer) === maximizingPlayer) {
			score += 900;
		}
		if (gameState.win(opponent) === opponent) {
			score -= 900;
		}

		// do not want to give opponent free move
		lastMove = gameState.lastMove;
		if (gameState.isFinal(lastMove.innerX, lastMove.innerY)) {
			score -= 450;
		}

		// points for each board won
		score += gameState.numWins(maximizingPlayer) * 150;
		score -= gameState.numWins(opponent) * 150;

		// points for each board with moves that win within 1 move
		score += gameState.potentialBoardWins(maximizingPlayer) * 100;
		score -= gameState.potentialBoardWins(opponent) * 100;

		// add current depth to score
		return score + depth;
	}

	// returns the best score for a player based on the current game state
	(function alphaBeta(gameState, depth, a, b, player) {
		var moves = [],
			newGameState,
			move,
			i;

		if (depth === 0 || gameState.over()) {
			return score(gameState, depth);
		}

		moves = gameState.getMoves();
		if (player === maximizingPlayer) {
			move = {score: -10000};
			for (i = 0; i < moves.length; i++) {
				newGameState = gameState.newStateFromMove(moves[i]);
				moves[i].score = alphaBeta(newGameState, depth - 1, a, b, opponent);
				move = max(move, moves[i]);
				a = max({score: a}, move).score;
				if (b <= a) {
					break;
				}
			}
		} else {
			move = {score: 10000};
			for (i = 0; i < moves.length; i++) {
				newGameState = gameState.newStateFromMove(moves[i]);
				moves[i].score = alphaBeta(newGameState, depth - 1, a, b, maximizingPlayer);
				move = min(move, moves[i]);
				b = min({score: b}, move).score;
				if (b <= a) {
					break;
				}
			}
		}

		bestMove = clone(move);
		return move.score;
	})(gameState, 8, -10000, 10000, maximizingPlayer);

	return bestMove;
}

// recieves messages from parent script
onmessage = function (e) {
	// import GameState
	importScripts('../GameState.js');

	// send best move to parent script
	postMessage(
		veryAI(new GameState({
			selected: e.data.selected,
			wonCells: e.data.wonCells,
			lastMove: e.data.lastMove,
			activePlayer: e.data.maximizingPlayer
		}), e.data.maximizingPlayer)
	);
};
